#define _USE_MATH_DEFINES	// use this to get access to cmath

#include <stdio.h>
#include <iostream>			// use this to get access to the std 
#include <string>			// use this to include strings
#include <sstream>			// use this to include string streams
#include <cmath>			// use this to include math functions and variables
#include "Vector.h"

using namespace std;		// use this to remove the std:: in front of a class member

#pragma region other classes
class Animal {
public:
	virtual void AnimalSound()
	{
		cout << "*Some Noise*" << endl;
	}
	virtual void Deliver() = 0;				// abstract method, must be overritten in the lower classes
protected:									// can only be called in this class or in classes which inherit from this base class
	void ProtectedMethod()
	{
		cout << "ProtectedMethod" << endl;
	}
private:
	void PrivateMethod()
	{
		cout << "PrivateMethod" << endl;
	}
};

class Circle {
private:
	float radius;
public:
	Circle()		// standard circle in case of no float parameter
	{
		radius = 1;
	}
	//Circle(float r)
	//{
	//	radius = r;
	//}
	Circle(float r) :radius(r) {}				// multiple values like this: :radius(r), something(z)
	void SetRadius(float r)
	{
		if (r > 0)
			radius = r;
	}
	float GetRadius() { return radius; }
	float surface()
	{
		return M_PI * radius * radius;
	}
};
class DeliveryService {
public:
	virtual void Deliver()
	{
		cout << "I�m gonna deliver sth!" << endl;
	}
};
// class Bird: protected Animal					// methods from enheritance public methods turn into protected methods in the next enheritance
// class Bird: private Animal					// methods from enheritance become private without exeption in the next enheritance
class Bird: public Animal, DeliveryService {	// inherits from Animal AND DeliveryService, methods stay public, protected and private in the next enheritance
private:
	string name;
public:
	Bird(string newName):name(newName){}
	void AnimalSound()
	{
		cout << "Kah! Kah!" << endl;
	}
	string GetName()
	{
		return this->name;
	}
	friend void Adopt(Bird& h);				// adopt has access due to being a friend to the class
	void Test()
	{
		ProtectedMethod();
	}
	void Deliver()
	{
		DeliveryService::Deliver();			// use the Deliver function from DeliveryService
	}
};
#pragma endregion

int Vector::dimension = 3;

#pragma region Main
int main()
{
#pragma region input/output
	//int a;
	//cin >> a;														// write an input into a variable
	//cout << "You have written " << a << " in here.\n";				// use cout instead of printf
#pragma endregion

#pragma region strings
	//string name;
	//cout << "What is your name?\n";
	//cin >> name;
	//string s = "Hello there, !\n";			// define a string
	//s += "General Kenobi!\n";				// add something to the string
	//s.insert(12, name);						// insert something in the string at a location
	//s.erase(0, 1);							// erase a number of chars in the string at a location
	//int pos = s.find("Kenobi");				// find a position of a string
	//cout << s;
	//cout << pos << "\n";

	//string::iterator i;						// gives out the whole string
	//for ( i = s.begin(); i < s.end(); i++)
	//{
	//	cout << *i;
	//}

	//// convert a string to a number
	//string number = "23";
	//istringstream stream(number);
	//int b;
	//stream >> b;
	//cout << b << "\n";

	//// convert a number to a string
	//string number2;
	//ostringstream stream2;
	//int c = 50;
	//stream2 << c;
	//number2 = stream2.str();
	//cout << number2 << "\n";
#pragma endregion

#pragma region classes  // does not work due to the new constructor
	//Circle firstCircle;
	//firstCircle.radius = 5;
	//cout << firstCircle.surface();
#pragma endregion

#pragma region constructors
	//Circle secondCircle(5);
	////Circle thirdCircle = 6;
	////Circle fourthCircle{7};
	////Circle fifthCircle = {8, anotherParameter};
	//cout << secondCircle.surface() << "\n";
	//secondCircle.radius = 10;
	//cout << secondCircle.surface();
#pragma endregion

#pragma region pointer to objects
	//Circle sixthCircle(20);
	//Circle* ptrtosixthCircle = &sixthCircle;
	//cout << ptrtosixthCircle->surface() << "\n";
	//ptrtosixthCircle->SetRadius(100);
	//cout << ptrtosixthCircle->surface() << "\n";
#pragma endregion

#pragma region operator overloading
	//Vector firstVector(1, 1, 1);
	//Vector secondVector(2, 2, 2);
	////Vector addedVector = firstVector.operator+(secondVector);
	//Vector addedVector = firstVector + secondVector;					// this is the same as the above
	//addedVector.PrintMe();
#pragma endregion

#pragma region friendship and inheritance
	//Bird chuck("Chuck");
	////Animal firstAnimal = chuck;							// this will print out the *Some Noise*
	//Animal* firstAnimal = &chuck;						// this will hold its old inheritance as a Bird
	//firstAnimal->AnimalSound();
	//Adopt(chuck);
	//chuck.AnimalSound();
	//chuck.Test();
	//cout << chuck.GetName();
#pragma endregion
	return 0;
}
#pragma endregion

#pragma region other functions
void Adopt(Bird& h)
{
	h.name = "Cracker";
}
void* run(int* tid)				// tid threadID
{
	cout << "working at" << *tid << endl;

}
#pragma endregion